//jshint esversion:6

const express = require("express");
const app = express();
const convertDigits = require('./digitsmm');
app.use(express.static("public"));
app.set('view engine', 'ejs');
const bodyParser = require("body-parser");
app.use(bodyParser.urlencoded({ extended: true }));
const request = require("request");
const mysql = require('mysql');
const fs = require('fs');
function getDateTime() {

    var date = new Date();

    var hour = date.getHours();
    hour = (hour < 10 ? "0" : "") + hour;

    var min = date.getMinutes();
    min = (min < 10 ? "0" : "") + min;

    var sec = date.getSeconds();
    sec = (sec < 10 ? "0" : "") + sec;

    var year = date.getFullYear();

    var month = date.getMonth() + 1;
    month = (month < 10 ? "0" : "") + month;

    var day = date.getDate();
    day = (day < 10 ? "0" : "") + day;

    return year + "-" + month + "-" + day + " " + hour + ":" + min + ":" + sec;

}

/*const mongoose = require("mongoose");
mongoose.connect('mongodb://zeyarpaing:zeyarpaing@cluster0-shard-00-00-ya5ms.mongodb.net:27017,cluster0-shard-00-01-ya5ms.mongodb.net:27017,cluster0-shard-00-02-ya5ms.mongodb.net:27017/test?ssl=true&replicaSet=Cluster0-shard-0&authSource=admin&retryWrites=true&w=majority',{useUnifiedTopology: true, useNewUrlParser: true, useCreateIndex: true });
const convidSchema = new mongoose.Schema({
    userIP: String,
    userTime: Date
});
const Convid = new mongoose.model("User",convidSchema);
*/
/*
var MongoClient = require('mongodb').MongoClient;
var assert = require('assert');

var db = null;
MongoClient.connect("mongodb+srv://zeyarpaing:zeyarpaing@cluster0-ya5ms.mongodb.net/test?retryWrites=true&w=majority", function(err,database) {
    assert.equal(err,null);
    db = database;
});

*/

app.get("/", function (req, res) {
    res.sendFile(__dirname + "/public/index.html");
    /*
    db.collection('accesses').find().toArray(function(err,doc) {
        assert.equal(err,null);
        console.log(doc);
        
    });
    
    client.connect(err => {
      const collection = client.db("test").collection("accesses");
      // perform actions on the collection object
      collection.insertOne({
          userIP: ""+req.connection.remoteAddress,
          userTime: new Date(getDateTime())
      });
      client.close();
    });
*/

});
app.get('/Map', (req, res) => {
    var toWrite = `var coronaCases = {
      data: {
        cases: {
          name: 'Total Cases',
          format: '{0}',
          thousandSeparator: ',',
          thresholdMax: 50000,
          thresholdMin: 0
        },
        active: {
          name: 'Active Cases',
          format: '{0}'
        },
        deaths: {
          name: 'Deaths',
          format: '{0}',
          thousandSeparator: ',',
          thresholdMax: 50000,
          thresholdMin: 1000
        },
        recovered: {
          name: 'Recovered',
          format: '{0}'
        },
        todayCases: {
          name: 'Today Cases',
          format: '{0}'
        },
        todayDeaths:{
          name: 'Today Deaths',
          format: '{0}'
        },
        casesPerMillion:{
          name: 'Cases Per Million',
          format: '{0}'
        },
        deathsPerMillion:{
          name: 'Deaths Per Million',
          format: '{0}'
        }
      },
      applyData: 'cases',\n`;
    let values = "values: {";

    request("https://corona.lmao.ninja/countries/", function (error, response, body) {
        if (!error && response.statusCode == 200) {
            let data = JSON.parse(body);
            for (key in data) {
                let iso2 = data[key].countryInfo.iso2;
                let cases = data[key].cases;
                let active = data[key].active;
                let death = data[key].deaths;
                let recovered = data[key].recovered;
                let todayCases = data[key].todayCases;
                let todayDeaths = data[key].todayDeaths;
                let casesPerMillion = data[key].casesPerOneMillion;
                let deathsPerMillion = data[key].deathsPerOneMillion;
                let tem = `\n${iso2}: {cases: ${cases}, active: ${active}, deaths: ${death}, recovered: ${recovered} ,todayCases:${todayCases}, todayDeaths:${todayDeaths}, casesPerMillion:${casesPerMillion}, deathsPerMillion:${deathsPerMillion} },`;
                values += tem;
            }
            values += `} };`;
            toWrite += values;
            fs.writeFile(__dirname + '/public/worldmap/data/' + "TEST.js", toWrite, (err) => {
                if (err) console.log(err);
                console.log("Successfully Written to File.");
            });
            res.send(`<script src="worldmap/dist/svgMap.js"></script>
            <div id="svgMapExample"></div>            
            <script src="worldmap/data/TEST.js"></script>
            <script>
                new svgMap({
                    targetElementID: "svgMapExample",
                    data: coronaCases
                });
            </script>`);
        }
    });

});
app.get('/Cases', function (req, res) {
    var filter = req.query.country;
    var filter_value = req.query.country || 'All';
    var country_datalist = `< option value = "USA" ></option > <option value="Italy"></option><option value="Spain"></option><option value="Germany"></option><option value="France"></option><option value="Iran"></option><option value="UK"></option><option value="Switzerland"></option><option value="Netherlands"></option><option value="Belgium"></option><option value="S. Korea"></option><option value="Turkey"></option><option value="Austria"></option><option value="Canada"></option><option value="Portugal"></option><option value="Israel"></option><option value="Norway"></option><option value="Brazil"></option><option value="Australia"></option><option value="Sweden"></option><option value="Czechia"></option><option value="Ireland"></option><option value="Denmark"></option><option value="Malaysia"></option><option value="Chile"></option><option value="Luxembourg"></option><option value="Ecuador"></option><option value="Poland"></option><option value="Japan"></option><option value="Romania"></option><option value="Pakistan"></option><option value="Philippines"></option><option value="Russia"></option><option value="Thailand"></option><option value="Indonesia"></option><option value="Saudi Arabia"></option><option value="Finland"></option><option value="South Africa"></option><option value="Greece"></option><option value="India"></option><option value="Iceland"></option><option value="Mexico"></option><option value="Panama"></option><option value="Dominican Republic"></option><option value="Peru"></option><option value="Singapore"></option><option value="Argentina"></option><option value="Serbia"></option><option value="Slovenia"></option><option value="Estonia"></option><option value="Croatia"></option><option value="Diamond Princess"></option><option value="Colombia"></option><option value="Hong Kong"></option><option value="Qatar"></option><option value="Egypt"></option><option value="New Zealand"></option><option value="UAE"></option><option value="Iraq"></option><option value="Morocco"></option><option value="Bahrain"></option><option value="Algeria"></option><option value="Lithuania"></option><option value="Armenia"></option><option value="Ukraine"></option><option value="Hungary"></option><option value="Lebanon"></option><option value="Latvia"></option><option value="Bulgaria"></option><option value="Bosnia and Herzegovina"></option><option value="Andorra"></option><option value="Costa Rica"></option><option value="Slovakia"></option><option value="Tunisia"></option><option value="Taiwan"></option><option value="Uruguay"></option><option value="Kazakhstan"></option><option value="Moldova"></option><option value="North Macedonia"></option><option value="Jordan"></option><option value="Kuwait"></option><option value="San Marino"></option><option value="Burkina Faso"></option><option value="Cyprus"></option><option value="Albania"></option><option value="Azerbaijan"></option><option value="Vietnam"></option><option value="Réunion"></option><option value="Oman"></option><option value="Ivory Coast"></option><option value="Faeroe Islands"></option><option value="Ghana"></option><option value="Malta"></option><option value="Uzbekistan"></option><option value="Senegal"></option><option value="Cameroon"></option><option value="Cuba"></option><option value="Honduras"></option><option value="Brunei"></option><option value="Afghanistan"></option><option value="Sri Lanka"></option><option value="Venezuela"></option><option value="Palestine"></option><option value="Nigeria"></option><option value="Mauritius"></option><option value="Channel Islands"></option><option value="Guadeloupe"></option><option value="Cambodia"></option><option value="Georgia"></option><option value="Bolivia"></option><option value="Belarus"></option><option value="Kyrgyzstan"></option><option value="Martinique"></option><option value="Montenegro"></option><option value="DRC"></option><option value="Trinidad and Tobago"></option><option value="Rwanda"></option><option value="Gibraltar"></option><option value="Paraguay"></option><option value="Mayotte"></option><option value="Liechtenstein"></option><option value="Aruba"></option><option value="Bangladesh"></option><option value="Monaco"></option><option value="French Guiana"></option><option value="Kenya"></option><option value="Isle of Man"></option><option value="Madagascar"></option><option value="Macao"></option><option value="Guatemala"></option><option value="Barbados"></option><option value="Uganda"></option><option value="Jamaica"></option><option value="Togo"></option><option value="El Salvador"></option><option value="French Polynesia"></option><option value="Zambia"></option><option value="Bermuda"></option><option value="Ethiopia"></option><option value="Congo"></option><option value="Mali"></option><option value="Niger"></option><option value="Djibouti"></option><option value="Maldives"></option><option value="Guinea"></option><option value="Haiti"></option><option value="New Caledonia"></option><option value="Bahamas"></option><option value="Tanzania"></option><option value="Cayman Islands"></option><option value="Equatorial Guinea"></option><option value="Eritrea"></option><option value="Mongolia"></option><option value="Dominica"></option><option value="Namibia"></option><option value="Saint Martin"></option><option value="Greenland"></option><option value="Myanmar"></option><option value="Syria"></option><option value="Grenada"></option><option value="Saint Lucia"></option><option value="Eswatini"></option><option value="Curaçao"></option><option value="Guyana"></option><option value="Laos"></option><option value="Libya"></option><option value="Mozambique"></option><option value="Seychelles"></option><option value="Suriname"></option><option value="Angola"></option><option value="Gabon"></option><option value="Zimbabwe"></option><option value="Antigua and Barbuda"></option><option value="Cabo Verde"></option><option value="Sudan"></option><option value="Benin"></option><option value="Vatican City"></option><option value="Sint Maarten"></option><option value="Nepal"></option><option value="Fiji"></option><option value="Mauritania"></option><option value="Montserrat"></option><option value="St. Barth"></option><option value="Gambia"></option><option value="Nicaragua"></option><option value="Bhutan"></option><option value="Turks and Caicos"></option><option value="CAR"></option><option value="Chad"></option><option value="Liberia"></option><option value="Somalia"></option><option value="MS Zaandam"></option><option value="Anguilla"></option><option value="Belize"></option><option value="British Virgin Islands"></option><option value="Guinea-Bissau"></option><option value="Saint Kitts and Nevis"></option><option value="Papua New Guinea"></option><option value="St. Vincent Grenadines"></option><option value="Timor-Leste"></option><option value="China"></option>`;
    var baseURL = `https://corona.lmao.ninja/all`;
    var countryURL = `https://corona.lmao.ninja/countries/`;
    let useURL;
    const countryList = [
        "usa",
        "italy",
        "spain",
        "germany",
        "france",
        "iran",
        "uk",
        "switzerland",
        "netherlands",
        "belgium",
        "s. korea",
        "turkey",
        "austria",
        "canada",
        "portugal",
        "israel",
        "norway",
        "brazil",
        "australia",
        "sweden",
        "czechia",
        "ireland",
        "denmark",
        "malaysia",
        "chile",
        "luxembourg",
        "ecuador",
        "poland",
        "japan",
        "romania",
        "pakistan",
        "philippines",
        "russia",
        "thailand",
        "indonesia",
        "saudi arabia",
        "finland",
        "south africa",
        "greece",
        "india",
        "iceland",
        "mexico",
        "panama",
        "dominican republic",
        "peru",
        "singapore",
        "argentina",
        "serbia",
        "slovenia",
        "estonia",
        "croatia",
        "diamond princess",
        "colombia",
        "hong kong",
        "qatar",
        "egypt",
        "new zealand",
        "uae",
        "iraq",
        "morocco",
        "bahrain",
        "algeria",
        "lithuania",
        "armenia",
        "ukraine",
        "hungary",
        "lebanon",
        "latvia",
        "bulgaria",
        "bosnia and herzegovina",
        "andorra",
        "costa rica",
        "slovakia",
        "tunisia",
        "taiwan",
        "uruguay",
        "kazakhstan",
        "moldova",
        "north macedonia",
        "jordan",
        "kuwait",
        "san marino",
        "burkina faso",
        "cyprus",
        "albania",
        "azerbaijan",
        "vietnam",
        "réunion",
        "oman",
        "ivory coast",
        "faeroe islands",
        "ghana",
        "malta",
        "uzbekistan",
        "senegal",
        "cameroon",
        "cuba",
        "honduras",
        "brunei",
        "afghanistan",
        "sri lanka",
        "venezuela",
        "palestine",
        "nigeria",
        "mauritius",
        "channel islands",
        "guadeloupe",
        "cambodia",
        "georgia",
        "bolivia",
        "belarus",
        "kyrgyzstan",
        "martinique",
        "montenegro",
        "drc",
        "trinidad and tobago",
        "rwanda",
        "gibraltar",
        "paraguay",
        "mayotte",
        "liechtenstein",
        "aruba",
        "bangladesh",
        "monaco",
        "french guiana",
        "kenya",
        "isle of man",
        "madagascar",
        "macao",
        "guatemala",
        "barbados",
        "uganda",
        "jamaica",
        "togo",
        "el salvador",
        "french polynesia",
        "zambia",
        "bermuda",
        "ethiopia",
        "congo",
        "mali",
        "niger",
        "djibouti",
        "maldives",
        "guinea",
        "haiti",
        "new caledonia",
        "bahamas",
        "tanzania",
        "cayman islands",
        "equatorial guinea",
        "eritrea",
        "mongolia",
        "dominica",
        "namibia",
        "saint martin",
        "greenland",
        "myanmar",
        "syria",
        "grenada",
        "saint lucia",
        "eswatini",
        "curaçao",
        "guyana",
        "laos",
        "libya",
        "mozambique",
        "seychelles",
        "suriname",
        "angola",
        "gabon",
        "zimbabwe",
        "antigua and barbuda",
        "cabo verde",
        "sudan",
        "benin",
        "vatican city",
        "sint maarten",
        "nepal",
        "fiji",
        "mauritania",
        "montserrat",
        "st. barth",
        "gambia",
        "nicaragua",
        "bhutan",
        "turks and caicos",
        "car",
        "chad",
        "liberia",
        "somalia",
        "ms zaandam",
        "anguilla",
        "belize",
        "british virgin islands",
        "guinea-bissau",
        "saint kitts and nevis",
        "papua new guinea",
        "st. vincent grenadines",
        "timor-leste",
        "china"
    ];
    if (filter) {
        if(countryList.indexOf(filter) in countryList)  useURL = countryURL + filter;
        else useURL = baseURL; 
    } else {
        useURL = baseURL;
    }
    
    let userip = "" + req.connection.remoteAddress;
    if (userip !== '::1') {
        const connection = mysql.createConnection({
            host: 'sql12.freesqldatabase.com',
            user: 'sql12330690',
            password: 'HSYnFi59M7',
            database: 'sql12330690'
        });

        let useragent = req.headers['user-agent'];
        let userhost = req.headers.host;

        let insertQ = `INSERT INTO anticonvid VALUES('${userip}', '${getDateTime()}','${useragent}','${userhost}')`;
        connection.query(insertQ);
        connection.end();
    }

    //console.log(filter,filter_value);
    request(useURL, function (error, response, body) {
        if (!error && response.statusCode == 200) {
            var data = JSON.parse(body);
            let total_cases = convertDigits.engToMM("" + data.cases);
            let acitve_cases = convertDigits.engToMM("" + data.active);
            let deaths = convertDigits.engToMM("" + data.deaths);
            let recovered = convertDigits.engToMM("" + data.recovered);

            let to_send = `<section class="filtersec">
                <label for="filter">နိုင်ငံအလိုက်ကြည့်ရန်</label>
                <input list="country" type="text" name="filter" id="filter" value="${filter_value}">
                 <button type="button" id="filterbtn" onclick="filterByCountry()">Filter</button>
            </section>
            <datalist id="country">
                ${country_datalist}
            </datalist>
            <div class="wrap">
                <section class="data-sec">
                    <h1>ကူးစက်ခံရသူ</h1>
                    <p id="total-case">${total_cases}</p>
                </section>
                <section class="data-sec">
                    <h1>လက်ရှိရောဂါရှိသူ</h1>
                    <p id="active-case">${acitve_cases}</p>
                </section>
                <section class="data-sec">
                    <h1>သေဆုံးသူ</h1>
                    <p id="deaths">${deaths}</p>
                </section>
                <section class="data-sec">
                    <h1>ရောဂါပျောက်ကင်းသူ</h1>
                    <p id="recovered">${recovered}</p>
                </section>
                
            </div>
            <cite>**ဤအချက်အလက်များသည်လျှင်မြန်စွာပြောင်းလဲနိုင်ခြင်း ရောဂါပိုးတွေ့မှုသတ်မှတ်ပုံစံသည် နိုင်ငံအလိုက်ကွဲပြားနိုင်ခြင်းတို့ကြောင့် ဖော်ပြရာတွင်အချက်အလက်နောက်ကျခြင်း၊ ကွဲပြားခြင်းများရှိနိုင်ပါသည် ။ ** <br>
            </cite>Source : 
            <a href='https://github.com/novelconvid/api'>NovelConvid API</a>`;
            res.send(to_send);
        }
    });

});
app.get('/Check', function (req, res) {
    let temp = `<iframe class="cases" src="/healthCheck" frameborder="0"></iframe>`;
    res.send(temp);
});
app.get('/healthCheck', function (req, res) {

    res.sendFile(__dirname + '/public/home.html');
});
app.post('/healthCheck', function (req, res) {
    var r = req.body;
    let points = [1, 1, 1, 1, 1, 1, 2, 2, 3, 3, 3];
    let dataList = [r.cough, r.cold, r.dia, r.throat, r.bodyaches, r.headaches, r.tired, r.breath, r.travel, r.infectedarea, r.nursing];
    let temp = r.temperature;
    let result = 0;
    for (let i = 0, len = dataList.length; i < len; i++) {
        if (dataList[i] === '1') {
            result += points[i];
        }
    }
    if (+temp > 99) result++;
    result = (result / (points.reduce((acc, cur) => acc + cur) + 1)) * 100;
    cases = {
        cond1: 'စိတ်ဖိစီးမှုကြောင့်ဖြစ်နိုင်ပြီး စောင့်ကြည့်ရန်လိုအပ်ပါသည်။',
        cond2: 'ရေများများသောက်ပါ၊ ကိုယ်ခံအားကောင်းအောင်နေပါ။ နောက်ထပ်(၂)ရက်စောင့်ကြည့်ရန်လိုပါသည်။',
        cond3: 'ဆရာဝန်နှင့်ဆွေးနွေးတိုင်ပင်ပါ။',
        cond4: 'ဆေးရုံ(သို့)ကျန်းမာရေးဌာန၏ စောင့်ကြည့်မှု(၁၄)ရက် ခံယူပါ။'
    };

    let message;
    if (result >= 60) {
        message = cases.cond4;
    } else if (result >= 30) {
        message = cases.cond3;
    } else if (result >= 15) {
        message = cases.cond2;
    } else if (result >= 5) {
        message = cases.cond1;
    } else {
        message = 'လက်ကိုမကြာခဏဆေးပါ၊ အပြင်မထွက်ဘဲအိမ်မှာသာနေပါ ';
    }
    let percent = convertDigits.engToMM(result + "");
    const resultF = "သင့်တွင်ရောဂါဖြစ်နိုင်ချေ  " + percent + "%  ရာနှုန်းရှိပါသည်။ ";
    let resultColor;
    if (result <= 5) {
        resultColor = 'green';
    } else {
        resultColor = 'red';
    }

    res.render('checkResult', { resultData: resultF, resultMessage: message, color: resultColor });
});
app.get('/Follow', function (req, res) {
    let to_send = `<main id="newsMain">
    <section class="follow-item red">
      <h2>လူစုလူဝေး နှင့် ပွဲတော်များကိုရှောင်ပါ။</h2>
      <p>
        လူထူထပ်သောနေရာများ ကိုရှောင်ကြဉ်ပါ။ မဖြစ်မနေသွားရမည်ဆိုပါက
        တစ်ဦးနှင့်တစ်ဦး အနည်းဆုံး ၃ ပေအကွာတွင်နေပါ။ Mask တပ်ပါ။
        အတက်နိုင်ဆုံးမိမိအိမ်၌သာနေပါ။
      </p>
    </section>
    <section class="follow-item red">
      <h2>ခရီးမထွက်ပါနှင့်  </h2>
      <p>
        မည်သည့်ဒေသကိုမဆိုခရီးထွက်ခြင်း‌ရှောင်ကြဉ်ပါ။ ခရီးထွက်ခြင်းကြောင့်
        သင်ရောဂါကူးစက်ခံရနိုင်ပါသည် ၊ တစ်ဖက်တွင်လည်း
        သင်သည်ကူးစက်ခံထားရသူဆိုလျှင် ရောဂါသယ်ဆောင်သူဖြစ်နိုင်ပါသည်။
      </p>
    </section>
    <section class="follow-item green">
      <h2>လက်ကိုမကြာခဏဆေးပေးပါ။</h2>
      <p>
        လက်ကိုဆပ်ပြာဖြင့် ၂၀ စက္ကန့်ခန့်ကြာအောင် စနစ်တကျဆေးကြောပေးပါ။
        မျက်စိ၊နှာခေါင်း၊ပါးစပ် တို့ကိုမလိုအပ်ဘဲမကိုင်ပါနှင့်။
        လက်ဆေးရန်မလွယ်ကူသောအခြေအနေတွင် လက်သန့်ဆေးရည် (Hand Sanitizer)
        ကိုသုံးပါ။
      </p>
    </section>
    <section class="follow-item green">
      <h2>စောင့်ကြည့်ခြင်းကိုကြည်ဖြူစွာခံယူပါ</h2>
      <p>
        ‌ရောဂါလက်က္ခာကြောင့်ဖြစ်စေ၊ နိုင်ငံခြားမှပြန်လာခြင်းကြောင့်ဖြစ်စေ
        သက်ဆိုင်ရာ ကျေးရွာ/ရပ်ကွက် (သို့မဟုတ်) ကျန်းမာရေးဌာန၏
        စောင့်ကြည့်ခြင်းကိုကြည်ဖြူစွာခံယူပါ။
      </p>
    </section>
    <section class="follow-item green">
      <h2>ကိုယ်ခံအားကောင်းအောင်နေပါ</h2>
      <ul>
        <li>ဆေးလိပ်မသောက်ပါနှင့်။</li>
        <li>အရက်သောက်တတ်ပါက လျော့သောက်ရန်။</li>
        <li>အာဟာရရှိပြီး လတ်ဆတ်သော အသီးအနှံများ ဟင်းသီးဟင်းရွက်များကိုစားပါ။</li>
        <li>ပုံမှန်လေ့ကျင့်ခန်းလုပ်ပါ။</li>
        <li>အိပ်ရေးဝအောင်အိပ်ပါ။</li>
        <li>စိတ်ဖိစီးမှုလျော့ချပါ။</li> 
      </ul>
    </section>
     <p class="share">သတင်းအမှားများမယုံကြရန်နှင့် ဤအချက်အလက်များကို သင့်မိသားစုနှင့်မိတ်ဆွေများကို ပြန်လည်မျှဝေပေးပါ။</p>
    </main>`;
    res.send(to_send);
});
/*
app.get('/News',(req,res)=>{
    res.sendFile(__dirname+"/public/news.html");
});
*/

app.get('/News', function (req, res) {
    var pageNo = req.query.page || 0;
    var searchKeyword = 'coronavirus';
    //console.log(pageNo);
    var authID = 'dvSVHqEuJhrF6tFalkMIdRSgJiOAOmor';
    var baseURL = `https://api.nytimes.com/svc/search/v2/articlesearch.json?q=${searchKeyword}&page=${pageNo}&api-key=${authID}`;
    var news = [];
    request(baseURL, function (error, response, body) {
        if (!error && response.statusCode == 200) {
            var data = JSON.parse(body);
            //console.log(data.status);

            var finalArticle = "";
            for (let i = 0; i < 10; i++) {
                let heading = data.response.docs[i].headline.main;
                let abstract = data.response.docs[i].lead_paragraph;
                let datetime = data.response.docs[i].pub_date;
                let author = data.response.docs[i].byline.original;
                let url = data.response.docs[i].web_url;
                let s = `<article class="news-article" onclick="loadNews('${url}')" title="Click to read more!">
                    <header>${heading}</header>
                    <cite>${author}</cite>
                    <section>
                        ${abstract}
                    </section>
                    <date>${datetime}</date>
                </article>`;

                finalArticle += s;
            }

            if (req.query.page) {
                res.send(finalArticle);
            } else {
                res.render("news", { data: finalArticle });
            }


        }
    });

});

/*
app.post("/", function(req,res){
    var firstName = req.body.fname;
    var lastName = req.body.lname;
    var email = req.body.email;
    
    var data = {
        members: [
            {
                email_address: email,
                status: "subscribed",
                merge_fields:{
                    "FNAME": firstName,
                    "LNAME": lastName
                }
            },
        ]
    };
 
    var jsonData = JSON.stringify(data);
    console.log(jsonData);
    var options = {
        url: "https://us20.api.mailchimp.com/3.0/lists/ebb4486218",
        method: "POST",
        headers: {
            "Authorization": "zeyarpaing 5b919e9879868a91ed39335cc9044200-us20"
        },
        body: jsonData
    };
 
    request(options , function(error, response ,body){
        if(error){
            res.sendFile(__dirname+"/error.html");
        }else if(response.statusCode === 200){
            res.sendFile(__dirname + "/thanks.html");
        }else {
            res.sendFile(__dirname + "/error.html");
        }
    }); 
});
*/
app.listen(process.env.PORT || 3000, function () {
    console.log("Sever Started at port 3000");
});

//Mailchimp
//5b919e9879868a91ed39335cc9044200-us20 API Key
//ebb4486218 List ID