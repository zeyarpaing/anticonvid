self.addEventListener('install', function(e) {
 e.waitUntil(
   caches.open('video-store').then(function(cache) {
     return cache.addAll([
        '/icons/icon.svg',
       '/index.html',
       '/home.html',
       '/fonts/font.css',
       '/fonts/Myanmar3.woff',
       '/fonts/Pyidaungsu Regular.woff',
       '/fonts/Pyidaungsu Bold.woff',
       '/style.css',
       '/jquery.js',
       '/icons/info.svg',
       '/icons/case-study.svg',
       '/icons/check-up.svg',
       '/icons/correct.svg',
       '/icons/eye.svg',
       '/images/exercise.svg',
       '/images/home.svg',
       '/images/social_distance.svg',
       '/images/wash_hand.svg',
       '/images/watch.svg'
     ]);
   })
 );
});

self.addEventListener('fetch', function(e) {
  console.log(e.request.url);
  e.respondWith(
    caches.match(e.request).then(function(response) {
      return response || fetch(e.request);
    })
  );
});