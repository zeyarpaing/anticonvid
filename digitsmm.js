//jshint esversion:6
function engToMM(engDig)
{
    digArr = Array.from(engDig);
    let mmCode = digArr.map(dig => String.fromCharCode(dig.charCodeAt(0) + 4112));
    return mmCode.join("");
}
function mmToEng(mmDig)
{
    digArr = Array.from(mmDig);
    let engCode = digArr.map(dig => String.fromCharCode(dig.charCodeAt(0) - 4112));
    return engCode.join("");
}
function timeConverter(UNIX_timestamp){
  var a = new Date(UNIX_timestamp * 1000);
  var months = ['Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec'];
  var year = a.getFullYear();
  var month = months[a.getMonth()];
  var date = a.getDate();
  var hour = a.getHours();
  var min = a.getMinutes();
  var sec = a.getSeconds();
  var time = date + ' ' + month + ' ' + year + ' ' + hour + ':' + min + ':' + sec ;
  return time;
}
module.exports = {engToMM,mmToEng,timeConverter};